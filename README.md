# Test Stress Project Service

The idea of this repository is to bootstrap to perform a stress test for the project service. 

In order to do that we need:

1. One virtual private server (VPS), for example an EC2 from AWS
2. Open certain ports for remote access
3. Docker suite is installed in the VPS
4. The service that we want to test
    

For example, we'll stress test the integration between the project svc and its volume.
Therefore, you need to first follow the instructions from [test-integration-svc-project](https://gitlab.com/binus-student-project-showcase/test-integration-svc-project), but you need to do it inside the VPS.
Don't forget to allow incoming requests to port `8080`. 
As long as you can access it from http://\<your vps public address\>:8080/, you'll just do fine.

## Stress Test with [k6](https://k6.io/)

I use the following version.
```
k6 v0.40.0 (2022-09-08T09:06:02+0000/v0.39.0-92-gdcbe2f9c, go1.18.6, linux/amd64)
```

### Steps
1. Change the URI
    
    Please make sure that you have changed the URI path accordingly

2. Test the GET endpoint

    ```sh
    k6 run --vus 10 --duration 30s http_get.js 
    ```

    And, the result can look like this (Notice that 0.03% total requests are failed):

    ```
       
             /\      |‾‾| /‾‾/   /‾‾/   
        /\  /  \     |  |/  /   /  /    
       /  \/    \    |     (   /   ‾‾\  
      /          \   |  |\  \ |  (‾)  | 
     / __________ \  |__| \__\ \_____/ .io
   
     execution: local
        script: http_get.js
        output: -
   
     scenarios: (100.00%) 1 scenario, 10 max VUs, 1m0s max duration    (incl. graceful stop):
              * default: 10 looping VUs for 30s (gracefulStop: 30s)
   
   
   running (0m51.1s), 00/10 VUs, 38067 complete and 0 interrupted    iterations
   default ✓ [======================================] 10 VUs  30s
   
        data_received..................: 30 MB  583 kB/s
        data_sent......................: 5.3 MB 104 kB/s
        http_req_blocked...............: avg=3.68µs  min=229ns  med=1.   68µs  max=6.51ms   p(90)=3.33µs  p(95)=4.61µs 
        http_req_connecting............: avg=1.08µs  min=0s        med=0s      max=4.49ms   p(90)=0s      p(95)=0s     
        http_req_duration..............: avg=8.4ms   min=2.07ms med=3.   72ms  max=31.39s   p(90)=7.56ms  p(95)=9.2ms  
          { expected_response:true }...: avg=6.78ms  min=2.07ms med=3.   72ms  max=31.39s   p(90)=7.55ms  p(95)=9.19ms 
        http_req_failed................: 0.03%  ✓ 13         ✗ 38054
        http_req_receiving.............: avg=33.87µs min=3.62µs med=29.   15µs max=757.57µs p(90)=47.94µs p(95)=69.38µs
        http_req_sending...............: avg=9.74µs  min=1.2µs  med=7.   97µs  max=527.38µs p(90)=14.59µs p(95)=23.6µs 
        http_req_tls_handshaking.......: avg=0s      min=0s        med=0s      max=0s       p(90)=0s      p(95)=0s     
        http_req_waiting...............: avg=8.36ms  min=2.03ms med=3.   67ms  max=31.39s   p(90)=7.52ms  p(95)=9.16ms 
        http_reqs......................: 38067  744.374157/s
        iteration_duration.............: avg=8.45ms  min=2.13ms med=3.   77ms  max=31.39s   p(90)=7.61ms  p(95)=9.25ms 
        iterations.....................: 38067  744.374157/s
        vus............................: 1      min=1        max=10 
        vus_max........................: 10     min=10       max=10 
    ```

3. Test the POST endpoint

    ```sh
    k6 run --vus 5 --duration 10s http_post.js  
    ```

    ```
      
            /\      |‾‾| /‾‾/   /‾‾/   
       /\  /  \     |  |/  /   /  /    
      /  \/    \    |     (   /   ‾‾\  
     /          \   |  |\  \ |  (‾)  | 
    / __________ \  |__| \__\ \_____/ .io
  
    execution: local
       script: http_post.js
       output: -
  
    scenarios: (100.00%) 1 scenario, 5 max VUs, 40s max duration (incl.     graceful stop):
             * default: 5 looping VUs for 10s (gracefulStop: 30s)
  
  
    running (10.0s), 0/5 VUs, 5040 complete and 0 interrupted iterations
    default ✓ [======================================] 5 VUs  10s
  
       data_received..................: 877 kB 88 kB/s
       data_sent......................: 1.2 MB 120 kB/s
       http_req_blocked...............: avg=8.46µs  min=245ns  med=2.    37µs  max=5.47ms   p(90)=6.05µs  p(95)=6.51µs  
       http_req_connecting............: avg=3.73µs  min=0s         med=0s      max=3.8ms    p(90)=0s      p(95)=0s      
       http_req_duration..............: avg=9.73ms  min=5.52ms med=9.    37ms  max=26.35ms  p(90)=12.4ms  p(95)=13.56ms 
         { expected_response:true }...: avg=9.73ms  min=5.52ms med=9.    37ms  max=26.35ms  p(90)=12.4ms  p(95)=13.56ms 
       http_req_failed................: 0.00%  ✓ 0         ✗ 5040
       http_req_receiving.............: avg=46.33µs min=3.77µs med=36.    13µs max=910.67µs p(90)=95.1µs  p(95)=103.61µs
       http_req_sending...............: avg=21.77µs min=1.56µs med=18.    07µs max=523.18µs p(90)=40.98µs p(95)=43.9µs  
       http_req_tls_handshaking.......: avg=0s      min=0s         med=0s      max=0s       p(90)=0s      p(95)=0s      
       http_req_waiting...............: avg=9.67ms  min=5.47ms med=9.    31ms  max=26.29ms  p(90)=12.33ms p(95)=13.45ms 
       http_reqs......................: 5040   503.53057/s
       iteration_duration.............: avg=9.91ms  min=5.66ms med=9.    54ms  max=26.51ms  p(90)=12.59ms p(95)=13.76ms 
       iterations.....................: 5040   503.53057/s
       vus............................: 5      min=5       max=5 
       vus_max........................: 5      min=5       max=5
    ```

You could also check the CPU utilization with regard to this stress, for example, as shown below.

![CPU utilization](./images/cpu_utilization.png)

## Notes

### Important Notes
Don't forget to replace the server name in the docker-compose.yml as well as the MYSQL password with the correct inputs.

### EC2 User Data
In the user data, based on [Docker installation in Ubuntu](https://docs.docker.com/engine/install/ubuntu/), you can use:

```sh
#!/bin/sh

sudo apt-get update -y
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y

echo 'docker compose "$@"' > docker-compose
sudo chmod +x docker-compose
sudo cp docker-compose /usr/bin/ 
```


