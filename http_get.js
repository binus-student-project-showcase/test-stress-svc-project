import http from 'k6/http';

// k6 run --vus 10 --duration 30s http_get.js 

export default function () {
    http.get('http://ec2-108-137-6-200.ap-southeast-3.compute.amazonaws.com:8080/api/v1/projects');
}