import http from 'k6/http';
import { randomString, randomIntBetween  } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';

// k6 run --vus 5 --duration 10s http_post.js 

export default function () {
  const url = 'http://ec2-108-137-6-200.ap-southeast-3.compute.amazonaws.com:8080/api/v1/projects';
  const payload = JSON.stringify({
    project_name: randomString(randomIntBetween(5,10)),
    owner: randomString(randomIntBetween(5,10)),
  });

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  http.post(url, payload, params);
}
